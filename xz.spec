Name:           xz
Version:        5.6.4
Release:        1
Summary:        A free general-purpose data compreession software with LZMA2 algorithm
License:        GPL-3.0-only
URL:            https://tukaani.org/xz
Source0:        https://github.com/tukaani-project/xz/releases/download/v%{version}/%{name}-%{version}.tar.xz
Source1:        colorxzgrep.sh
Source2:        colorxzgrep.csh

# https://github.com/tukaani-project/xz/releases/tag/v5.4.7
# https://github.com/tukaani-project/xz/releases/download/v5.4.7/xz-5213-547-562-libtool.patch
#Patch0:         xz-5213-547-562-libtool.patch

BuildRequires:  perl-interpreter gcc

Requires:       %{name} = %{version}-%{release}
Requires:       grep >= 2.20-5

%description
XZ Utils is free general-purpose data compression software with a high compression ratio.
XZ Utils were written for POSIX-like systems, but also work on some not-so-POSIX systems. XZ Utils are the successor to LZMA Utils.

The core of the XZ Utils compression code is based on LZMA SDK, but it has been modified quite a lot to be suitable for XZ Utils.
The primary compression algorithm is currently LZMA2, which is used inside the .xz container format. With typical files, XZ Utils create 30% smaller output than gzip and 15% smaller output than bzip2.

%package        devel
Summary:        Libraries & headers for xz
Requires:       %{name} = %{version}-%{release}
Provides:       xz-static = %{version}-%{release}
Obsoletes:      xz-static < %{version}-%{release}

%description    devel
This package mainly includes the following contents: static library,
the header file, example, tests use case, other development and use of content.

%package lzma-compat
Summary:        Old LZMA format compatibility binaries
Requires:       %{name} = %{version}-%{release}
Provides:       lzma = %{version}
Obsoletes:      lzma < %{version}

%description lzma-compat
This package contains the compatibility binaries for older LZMA.

%package        libs
Summary:        Libraries for xz
Obsoletes:      %{name}-compat-libs < %{version}-%{release}

%description    libs
Libraries for decoding files compressed with LZMA or XZ utils.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%disable_rpath
%make_build

%install
%make_install
%delete_la

# config color alias for xz*grep
%global profiledir %{_sysconfdir}/profile.d
mkdir -p %{buildroot}%{profiledir}
install -p -m 644 %{SOURCE1} %{buildroot}%{profiledir}
install -p -m 644 %{SOURCE2} %{buildroot}%{profiledir}

%find_lang %name

%check
LD_LIBRARY_PATH=$PWD/src/liblzma/.libs %make_build check

%files -f %{name}.lang
%doc %{_pkgdocdir}
%license COPYING*
%{_bindir}/*xz*
%{profiledir}/*
%exclude %_pkgdocdir/examples*

%files libs
%{_libdir}/lib*.so.5*

%files lzma-compat
%{_bindir}/*lz*

%files devel
%dir %{_includedir}/lzma
%doc %_pkgdocdir/examples*
%{_includedir}/lzma/*.h
%{_includedir}/lzma.h

%{_libdir}/pkgconfig/liblzma.pc
%{_libdir}/liblzma.a
%{_libdir}/*.so

%files help
%{_mandir}/man1/*
%lang(de) %{_mandir}/de/man1/*
%lang(fr) %{_mandir}/fr/man1/*
%lang(ko) %{_mandir}/ko/man1/*
%lang(ro) %{_mandir}/ro/man1/*
%lang(uk) %{_mandir}/uk/man1/*
%lang(pt_BR) %{_mandir}/pt_BR/man1/*

%changelog
* Fri Jan 24 2025 Funda Wang <fundawang@yeah.net> - 5.6.4-1
- update to 5.6.4

* Wed Oct 09 2024 Yu Peng <yupeng@kylinos.cn> - 5.6.3-1
- Upgrade version to 5.6.3 to fix CVE-2024-47611 

* Thu Aug 01 2024 Funda Wang <fundawang@yeah.net> - 5.4.7-1
- Update to 5.4.7

* Tue Apr 30 2024 kouwenqi <kouwenqi@kylinos.cn> - 5.4.4-2
- liblzma: Add overflow check for Unpadded size in lzma_index_append

* Fri Aug 4 2023 dillon chen <dillon.chen@gmail.com> - 5.4.4-1
- update version to 5.4.4

* Wed Jun 21 2023 dillon chen <dillon.chen@gmail.com> -5.4.3-1
- upgrade package version to 5.4.3

* Mon Jan 30 2023 wangjunqi <wangjunqi@kylinos.cn> - 5.4.1-1
- upgrade package version to 5.4.1

* Wed Jan 11 2023 renhongxun <renhongxun@h-partners.com> - 5.2.10-1
- Type:Bump version
- ID:NA
- SUG:NA
- DESC:upgrade to 5.2.10

* Sat Oct 29 2022 hkgy <kaguyahatu@outlook.com> - 5.2.7-1
- Type:Bump version
- ID:NA
- SUG:NA
- DESC:update to 5.2.7

* Fri Sep 23 2022 wangjiang <wangjiang37@h-partners.com> - 5.2.5-3
- Type:enhancement
- CVE:NA
- SUG:NA
- DESC:config color alias for xz*grep

* Fri Apr 15 2022 liudabo <liudabo1@h-partners.com> - 5.2.5-2
- Type:CVE
- ID:CVE-2022-1271
- SUG:NA
- DESC:Fix CVE-2022-1271

* Thu Jul 23 2020 shixuantong <shixuantong@huawei.com> - 5.2.5-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update to  5.2.5-1

* Sat Mar 21 2020 shenyangyang<shenyangyang4@huawei.com> - 5.2.4-10
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Split xz-lzma-compat subpackage

* Mon Feb 24 2020 chengquan<chengquan3@huawei.com> - 5.2.4-9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Split libs subpackage for xz

* Fri Feb 21 2020 chengquan<chengquan3@huawei.com> - 5.2.4-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add necessary BuildRequire

* Mon Jan 20 2020 JeanLeo<liujianliu.liu@huawei.com> - 5.2.4-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:correct patch info

* Mon Jan 20 2020 JeanLeo<liujianliu.liu@huawei.com> - 5.2.4-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revert the deleted patch

* Sat Jan 18 2020 JeanLeo<liujianliu.liu@huawei.com> - 5.2.4-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove useless file or patch

* Mon Sep 2 2019 dongjian<dongjian13@huawei.com> - 5.2.4-4
- Rebuild the xz and fix description
